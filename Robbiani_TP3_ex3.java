import java.io.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.parsers.*;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class Robbiani_TP3_ex3 {
	public static PrintWriter out = null;

	public static void main(String[] args) {
		try {
		System.out.println("debut");
		
		String filename = ".\\Exercice3.xml";
		
		convert(filename);
		
		System.out.println("fin");
		
		}
		catch (Exception e ){
			e.printStackTrace();
		}
		
	}
	
	
	//Rem changer Package par ModelElement
	public static void convert(String _fichier)
			throws SAXException, ParserConfigurationException, IOException, XPathExpressionException 
	{
	
			// Charger le document
			FileInputStream _xml_input_file = new FileInputStream("/Users/ety/Desktop/TP3/DOM/Exercice3.xml");
	
			convert(_xml_input_file);
	}

	public static void convert(InputStream _xml_input_file)
		throws SAXException, ParserConfigurationException, IOException, XPathExpressionException 
	{
		//creation du fichier output
		out = new PrintWriter( new FileOutputStream("/Users/ety/Desktop/TP3/DOM/Exercice3_Output.xml"));
		
		out.println("<?xml version=\"1.0\"?> <!DOCTYPE list [");
		out.println("<!ELEMENT list (man | woman)*>");
		out.println("<!ELEMENT man (sons, daughters)> ");
		out.println("<!ATTLIST man name CDATA #REQUIRED> ");
		out.println("<!ELEMENT woman (sons, daughters)> ");
		out.println("<!ATTLIST woman name CDATA #REQUIRED> ");
		out.println("<!ELEMENT sons (man)*>");
		out.println("<!ELEMENT daughters (woman)*>");
		out.println("]>");



		//instancier le contrcuteur de parseurs
		DocumentBuilderFactory _factory = DocumentBuilderFactory.newInstance();

		//ignorer les commentaires dans les fichiers XML parse
		_factory.setIgnoringComments(true);
		
		// creer un parseur
		DocumentBuilder _builder = _factory.newDocumentBuilder();

		// Charger le document
		Document doc = _builder.parse(_xml_input_file);

		//...... 
	
         Element root = doc.getDocumentElement();
         XPathFactory xpf = XPathFactory.newInstance();
         XPath path = xpf.newXPath();
         
         out.println("<list>");
         
         int expressionNumber = 1;
         String expression = "/list/person[" + expressionNumber + "]";
         
         while( (Boolean)path.evaluate(expression, root, XPathConstants.BOOLEAN) )
         {
        	 if((Boolean)path.evaluate(expression+"[@gender='M']", root, XPathConstants.BOOLEAN))
        	 {
        		 ManTemplate(expression, path, root);
        	 }
        	 else
        	 {
        		 WomanTemplate(expression, path, root);
        	 }
        	 out.println("");
        	 
        	 expressionNumber++;
        	 expression = "/list/person[" + expressionNumber + "]";
         }
		 
		 out.println("</list>");
		 
		//.......
		out.close();
		out.flush();
	}
	
	public static void ManTemplate(String expression, XPath path, Element root) throws XPathExpressionException 
	{
		out.println("<man name=\""+ (String)path.evaluate(expression+"/name", root) +"\"> ");
		
		int expressionNumber = 1;
		
		out.print("<sons>");
		while( (Boolean)path.evaluate(expression+"/children/person[@gender='M']["+ expressionNumber +"]", root, XPathConstants.BOOLEAN) )
		{
			if(expressionNumber == 1)
			{
				out.println("");
			}
			
			ManTemplate(expression+"/children/person[@gender='M']["+ expressionNumber +"]", path, root);
			
			expressionNumber++;
		}
		out.println("</sons>");
		
		expressionNumber = 1;
		
		out.print("<daughters>");
		while((Boolean)path.evaluate(expression+"/children/person[@gender='F']["+ expressionNumber +"]", root, XPathConstants.BOOLEAN))
		{
			if(expressionNumber == 1)
			{
				out.println("");
			}
			
			WomanTemplate(expression+"/children/person[@gender='F']["+ expressionNumber +"]", path, root);
			
			expressionNumber++;
		}
		out.println("</daughters>");
		out.println("</man>");
	}
	
	public static void WomanTemplate(String expression, XPath path, Element root) throws XPathExpressionException 
	{
		out.println("<woman name=\""+ (String)path.evaluate(expression+"/name", root) +"\"> ");
		int expressionNumber = 1;
		out.print("<sons>");
		while((Boolean)path.evaluate(expression+"/children/person[@gender='M']["+ expressionNumber +"]", root, XPathConstants.BOOLEAN))
		{
			if(expressionNumber == 1)
			{
				out.println("");
			}
			
			ManTemplate(expression+"/children/person[@gender='M']["+ expressionNumber +"]", path, root);
			
			expressionNumber++;
		}
		out.println("</sons>");
		
		expressionNumber = 1;
		
		out.print("<daughters>");
		while((Boolean)path.evaluate(expression+"/children/person[@gender='F']["+ expressionNumber +"]", root, XPathConstants.BOOLEAN))
		{
			if(expressionNumber == 1)
			{
				out.println("");
			}
			
			WomanTemplate(expression+"/children/person[@gender='F']["+ expressionNumber +"]", path, root);
			
			expressionNumber++;
		}
		out.println("</daughters>");
		out.println("</woman>");
	}

}
